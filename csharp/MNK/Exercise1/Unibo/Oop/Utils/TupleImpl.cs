﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private object[] objects;
        public TupleImpl(object[] args)
        {
            this.objects = args;
        }

        public object this[int i]
        {
            get => this.objects[i];
        }

        public int Length => this.objects.Length;

        public object[] ToArray()
        {
            object[] array = new object[objects.Length];
            Array.Copy(objects, array, objects.Length);
            return array;
        }

        public override string ToString()
        {
            return "(" + string.Join(", ", objects.Select(o => o.ToString()).ToArray()) + ")";
        }

        public override bool Equals(object o)
        {
            if(o == null || !(o is TupleImpl) || ((TupleImpl) o).Length != objects.Length)
            {
                return false;
            }
            return Enumerable.SequenceEqual(objects, ((TupleImpl) o).objects);
        }
        
        public override int GetHashCode()
        {
            int result = 1;
            foreach(var a in objects)
            {
                result = 31 * result + a.GetHashCode();
            }
            return result;
        }
    }
}
