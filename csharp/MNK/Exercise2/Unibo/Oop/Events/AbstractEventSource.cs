﻿using System;
using System.Collections.Generic;

namespace Unibo.Oop.Events
{
    public abstract class AbstractEventSource<TArg> : IEventEmitter<TArg>, IEventSource<TArg>
    {
        protected abstract ICollection<EventListener<TArg>> GetEventListeners();

        public void Emit(TArg data)
        {
            foreach (var eventListener in GetEventListeners())
            {
                eventListener.Invoke(data);
            }
        }

        public void Bind(EventListener<TArg> eventListener)
        {
            GetEventListeners().Add(eventListener);
        }

        public void Unbind(EventListener<TArg> eventListener)
        {
            GetEventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            GetEventListeners().Clear();
        }

        public IEventSource<TArg> EventSource => this;
 
    }    
}