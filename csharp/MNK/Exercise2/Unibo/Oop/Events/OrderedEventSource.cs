﻿using System;
using System.Collections.Generic;

namespace Unibo.Oop.Events
{
    public class OrderedEventSource<TArg> : AbstractEventSource<TArg>
    {
        private IList<EventListener<TArg>> eventListeners = new List<EventListener<TArg>>();

        protected override ICollection<EventListener<TArg>> GetEventListeners()
        {
            return eventListeners;
        }
    }
}